package edu.uchicago.gerber.lablec01a;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    private Button bntGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        bntGo = findViewById(R.id.btnGo);
        bntGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent expIntent =
                        new Intent(SecondActivity.this, MainActivity.class);


                Intent impIntent = new Intent(Intent.ACTION_VIEW);
                impIntent.setData(Uri.parse("https://www.google.com/"));


                //explicit intent example
                startActivity(expIntent);

                //implicity intent example
                //startActivity(impIntent);
            }
        });

    }
}
