package edu.uchicago.gerber.lablec01a;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    public static final String ONCLICKER = "onclicker";
    //1. create components corresponding to the layout components with ids
    private LinearLayout layRoot;
    private EditText edtName;
    private Button btnAlert;
    private Button btnColor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //inflate
        setContentView(R.layout.activity_main);



        //2. inflate the layout file and assign references to objects
        layRoot = findViewById(R.id.layRoot);
        edtName = findViewById(R.id.edtName);
        btnAlert = findViewById(R.id.btnAlert);
        btnColor = findViewById(R.id.btnColor);

        btnAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(ONCLICKER, "Hello " + edtName.getText().toString());
            }
        });

        btnColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Random random = new Random();

                int argb = Color.argb(
                        random.nextInt(255),
                        random.nextInt(255),
                        random.nextInt(255),
                        random.nextInt(255)

                );
                layRoot.setBackgroundColor(argb);

                Log.d(ONCLICKER, Integer.toHexString(argb));

            }
        });




    }


}
